# Mugatiya's Player Peak Analyzer - How To and Stuff #

Conceptualised and specified by Mugatiya, INC. and from the mind of just Mugatiya.
* Small Print: [Developed by an OK programmer].

### What is this application for? ###

* Find a cricketer's best run of consecutive matches.
* For both batsmen and bowlers.
* 'Best' can be found as either best average or most runs/wickets.

### Minimum requirements and Installation ###

* Windows 7 or above. I haven't tested on anything below.
* .NET 4.0 or above. If you don't know what that is, don't worry you probably have that.
* Copy and paste both "CricketPlayerPeakAnalyzer.exe" and "PlayerPeakLibrary.dll" into the same folder.

	** It MUST be the same folder.
* Have write permission to that folder.

### User Guide ###

* You're going to need to do a little bit of Statsguru for this. 

	** Link: [Statsguru](http://stats.espncricinfo.com/ci/engine/stats/index.html)
* Bless 'em, Statsguru folk.

#### Find a Batting Peak ####
* Pull up player profile
* A little ways down there's a "Test statistics" drop down box. From it select "Batting innings list". This is VERY important.
* From the resulting list, copy paste ONLY the stats (not column headers) into a text editor like Notepad or Sublime Text, and save it with the player's name and file extension ".tsv".
	** For example, Sangakkara.tsv
* Prepare as many files like this as you want, one per player. Save them with the players' names and put in the same folder.
* Extract the CricketPlayerPeakAnalyzer.zip file.
* Run the CricketPlayerPeakAnalyzer.exe
* Drag-and-Drop the folder with the preapred files into the "Path" text box, or use the "Browse" button to browse to it.
* Since you're finding out batting peak, make sure "Type" radio button is set to "Batting".
* You can choose the best peak depending on one of two criteria:
	** The peak with the best batting average or the peak with the most runs.
* "Result Criteria" radio button lets you select this. By default it's set to "Average".
* "Open Output File" checkbox will open the resulting output file with whatever your OS is set to open .tsv files with.
* "Peak Length" is set to 33 by default. This is the length of the peak the application will analyze. You can change it to any value you like.
	** If it is larger than the length of a player's career, output file will contain an error for that player.
* Hit "FIND PEAK!"

#### Find a Bowling Peak ####
* Pretty much the same except for these differences.
* In player profile, select "Bowling innings list".
* Place bowler stat files in a folder that's different to the batsmen stats folder.
* When the application is run, for "Type" select "Bowling".
* The rest is the same.

### Sample Files ###
* I have included a few sample input files for both batsmen and bowlers.
* Check them out before you prepare your own files to get an idea.

### Warnings ###
* If the Output file from a previous file is open, the application will fail to overwrite it and generate an erro. This is an easy mistake to make.
* Don't mix up batting and bowling stats.
* I wrote this in like few hours, so I'm sure there are bugs. If you find any, shoot me an email.

### Who do I talk to? ###

* Admin: Sach
* EMail me at sachintha81[at]gmail.com